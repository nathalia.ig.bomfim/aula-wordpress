<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mLATWcx61YNjdNnYB6s8lePCgLdiTXKq3itqGl1dKb3/rw9YdtDUVGJduijgnx/U8gj5g3PHFxDFUpJKTdEKpA==');
define('SECURE_AUTH_KEY',  'TR7/s6jAKNiSclP05ylwdPQLh/CZitbBcxYT7utpuoBG8Uo0eIWKH0FuuJIx9KojnInA8Z5Ba8BdXL/fAV+rsA==');
define('LOGGED_IN_KEY',    '7UIiL5O/tqUB6fDMnpQiRMjQQS2TwCTmTa887HL3AN/T6Zyb65KCCe+sRZmSw5Go5oKMYVsd87Kz9K2XviAooA==');
define('NONCE_KEY',        '5tRIcC7yiiz5biAW4rKKh+sKY+LyxY2p1kQwTqRM4ae8GMG76ensk4gLhQf5h9w6Niehf15JT361Ic4MHUuayQ==');
define('AUTH_SALT',        'ZH+M4xG/+85e3Ape7X30VV42oImlYlU6ROQCpg0+AquF4qhts5+Vr+nDOcq+WbJDm0PzyncDJ21QCOpq2+MWkQ==');
define('SECURE_AUTH_SALT', '3t+TcLcO0UcMxDkDBK9z5ZIEseRGpuTfeIzSxQhgbcgH4rJOUX0NNvUX7f9p3NhNuQBMy2myoeOH2z4ROt9LOw==');
define('LOGGED_IN_SALT',   '15HG15aVKSD/d2dLz9c5VPl8KREaokBjJCrNMao7a3c3rzo0MBUjRZEFL0taz14Vd+LKfno+LxkdVXZ6PzFV4A==');
define('NONCE_SALT',       'y9RaL+KIRtejLn4CUfb1pSZRn8IOjhoBIvfiZvcO5m5I6wpi9FzH+/mFSCulp9T6r7qdHnEPyi0H4hioJu+XVQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
