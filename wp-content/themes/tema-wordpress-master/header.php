<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Título do site</title>
    <link rel="icon" href="<?php echo get_template_directory_uri()?>/assets/img/logo-in-junior.png">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/assets/css/reset.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/style.css">
    <?php wp_head()?>
</head>
<body>
    <header>
        <nav class="container">
            <a href="#"><img src="<?php echo get_template_directory_uri()?>/assets/img/logoIN.svg" alt="logo"></a>
            <input type="checkbox" id="checkconfig" style="display: none"/>
            <label for="checkconfig" class="nav-icon">
                <div></div>
            </label>
            <ul id="menu-navegacao" class="menu">
                <li ><a href="<?php echo get_template_directory_uri()?>/servicos.html">Serviços</a></li>
                <li ><a href="<?php echo get_template_directory_uri()?>/noticias.html">Notícias</a></li>
                <li ><a href="<?php echo get_template_directory_uri()?>/contato.html">Contato</a></li>
            </ul>
        </nav>
    </header>