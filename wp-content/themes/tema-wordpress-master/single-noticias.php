<?php get_header() ?>

<main id="single-noticia" class="container">
        <div class="card">
            <div>
                <h1>Título da Notícia</h1>
                <div>
                    <p>Criado em: XX/XX/XXXX</p>
                    <p>Editado em: XX/XX/XXXX</p>
                </div>
            </div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda sed tempore, laudantium explicabo magni velit ex! Quos exercitationem voluptates porro accusamus. Alias ipsum repellat dolores impedit maiores esse, hic doloremque!</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis minus quas ratione sequi vero optio officia dolor! Asperiores sequi atque ipsa eligendi fugiat, eveniet doloribus esse? Dolore sed dolor laudantium.</p>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magnam quod fugiat rerum odio repudiandae nulla id libero possimus at. Dolore, adipisci? Iusto, dolorum quae. Illum sunt harum exercitationem dolore excepturi!</p>
        </div>
        <div id="side-news">
            <h3>Artigos Relacionados</h3>
            <div>
                <div class="card">
                    <h4>Título da Notícia</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas odio doloremque quidem suscipit necessitatibus earum provident! Facere reprehenderit eum ad saepe deserunt, aut, incidunt molestiae soluta pariatur laboriosam praesentium atque.</p>
                    <a href="#">Continuar Lendo</a>  
                </div>
            </div>
        </div>
    </main>

<? get_footer() ?>