<?php get_header() ?>

<main id="all-noticias" class="container">
        <h1>Todas as Notícias</h1>
        <!-- <p>Na há notícias disponíveis!</p> -->
        <div class="news">
            <div class="card">
                <h2>Título da Notícia</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque deleniti dignissimos corporis a repellendus consequatur sed eligendi. Sapiente quidem molestias veniam voluptatum placeat! Earum, numquam excepturi placeat praesentium ea reprehenderit.</p> 
                <a href="#">Continuar Lendo</a> 
            </div>
            <div class="card">
                <h2>Título da Notícia</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque deleniti dignissimos corporis a repellendus consequatur sed eligendi. Sapiente quidem molestias veniam voluptatum placeat! Earum, numquam excepturi placeat praesentium ea reprehenderit.</p> 
                <a href="#">Continuar Lendo</a> 
            </div>
            <div class="card">
                <h2>Título da Notícia</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque deleniti dignissimos corporis a repellendus consequatur sed eligendi. Sapiente quidem molestias veniam voluptatum placeat! Earum, numquam excepturi placeat praesentium ea reprehenderit.</p> 
                <a href="#">Continuar Lendo</a> 
            </div>
            <div class="card">
                <h2>Título da Notícia</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque deleniti dignissimos corporis a repellendus consequatur sed eligendi. Sapiente quidem molestias veniam voluptatum placeat! Earum, numquam excepturi placeat praesentium ea reprehenderit.</p> 
                <a href="#">Continuar Lendo</a> 
            </div>
            <div class="card">
                <h2>Título da Notícia</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque deleniti dignissimos corporis a repellendus consequatur sed eligendi. Sapiente quidem molestias veniam voluptatum placeat! Earum, numquam excepturi placeat praesentium ea reprehenderit.</p> 
                <a href="#">Continuar Lendo</a> 
            </div>
            <div class="card">
                <h2>Título da Notícia</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque deleniti dignissimos corporis a repellendus consequatur sed eligendi. Sapiente quidem molestias veniam voluptatum placeat! Earum, numquam excepturi placeat praesentium ea reprehenderit.</p> 
                <a href="#">Continuar Lendo</a> 
            </div>
        </div>
        <div class="paginate-links">
        </div>
    </main>

<?php get_footer() ?>