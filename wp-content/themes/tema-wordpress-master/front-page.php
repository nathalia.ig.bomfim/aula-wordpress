<?php get_header() ?>

<main id="homepage">     
        <section id="sct-1">
            <h1>Meu Site</h1>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Error facilis saepe perspiciatis accusamus cum incidunt culpa pariatur nesciunt.</p>  
            <img src="<?php echo get_template_directory_uri()?>/assets/img/NOG_8186-Edit.jpg" alt="">      
        </section>
        <section id="sct-2" class="container">
            <div class="card">
                <div><h2>Missão</h2></div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima explicabo amet iure tempore possimus facere ipsum, earum neque corrupti ex ipsa sapiente quam architecto ratione quo? Pariatur aperiam nesciunt odit.</p>
            </div>
            <div class="card">
                <div><h2>Visão</h2></div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium, accusantium dolorem ex veritatis eaque, quia vero voluptas voluptatum quisquam pariatur reiciendis culpa sit placeat asperiores minus sapiente dolorum. Eos, ipsum?</p>
            </div>
        </section>
        <section id="sct-3" class="container">
            <h2>Nossos Serviços</h2>
            <div class="container">
                <div class="card"><h3>Foco</h3></div>
                <div class="card"><h3>Força</h3></div>
                <div class="card"><h3>Fé</h3></div>
            </div>
            <a href="<?php echo get_template_directory_uri()?>/servicos.html">Saiba mais</a>
        </section>
    </main>